#!/bin/bash

#set -x verbose #echo on


SEPARATOR=';'

function usage {
	echo "Modo de uso:"
	echo "$0 [-i linksFile -d destinationDir [-c cookiesFile]]"
	echo ""
	echo "linksFile es un archivo con un enlace por linea, con el siguiente formato:"
	echo "   url[;fileName]"
	echo ""
	echo "destinationDir es el directorio donde se ubicaran los archivos descargados."
	echo ""
	echo "cookiesFile es un archivo que contiene la salida del plugin 'cookies.txt export' de Google Chrome."
	echo "	Este archivo es necesario en los casos de descargas desde sitios que requieren login."
	echo ""
	echo "Para ver esta ayuda nuevamente, invocar sin parametros."
	exit 1
}

function errorMsg {
	echo "ERROR: $1"
	exit 2
}

######################################################################################
# Parseo de parametros:
######################################################################################
# http://www.unix.com/shell-programming-scripting/174105-how-make-shell-script-arguments-optional.html

# El archivo tiene un enlace por linea, con un campo opcional
# (separado por SEPARATOR del enlace) indicando el nombre del
# archivo a crear con la descarga.
inputFile=''

destinationDir=''

# Para los casos en que es necesario loguearse al sitio para
# descargar, hacerlo desde chrome y utilizar el plugin de las
# cookies para wget.
cookiesFile=''

while [ $# -gt 0 ]
do
	case "$1" in
		-i) inputFile="$2" ; shift ;;
		-d) destinationDir="$2" ; shift ;;
		-c) cookiesFile="$2" ; useCookies=true; shift ;;
		--) shift; break ;;
		-*) usage ;;
		*) break ;;  # arguments ...
	esac
	shift
done
# now $* include arguments
# Check required args
[ "$inputFile" = "" ] && usage $@
[ "$destinationDir" = "" ] && usage $@

echo "-------------------------------------------------------------"
echo "Lista de enlaces     : ${inputFile}"
echo "Directorio de destino: ${destinationDir}"
[[ $useCookies ]] && echo "Archivo de cookies   : ${cookiesFile}"
echo "-------------------------------------------------------------"


######################################################################################
# Validaciones
######################################################################################
# TODO - Chequear formato de archivos...

[[ -f ${inputFile} ]]                      || errorMsg "No existe el archivo con la lista de enlaces."
[[ ! ${useCookies} || -f ${cookiesFile} ]] || errorMsg "No existe el archivo de cookies."
mkdir -p ${destinationDir}; [[ $? == 0 && -w ${destinationDir} ]]  || errorMsg "No se pudo crear el directorio de destino."



######################################################################################
# Ejecucion
######################################################################################

echo "Comienzo de la descarga por lotes"
while IFS=$SEPARATOR read -ra line; do
	url="${line[0]}"
	name="${line[1]}"

	echo "##################################################################"
	echo "Descargando ${url}"
	echo "##################################################################"

	# -t inf para que intente hasta que lo logre, no importa la cantidad de veces
	# -c para que continue la descarga desde donde la dejo
	# -O si se indica un nombre para
	if [[ $useCookies ]];
	then
		wget --load-cookies $cookiesFile -t inf -c ${url} ${name:+-O "$name"} -P ${destinationDir}
	else
		wget -t inf -c ${url} ${name:+-O "$name"} -P ${destinationDir}
	fi
done < $inputFile
echo "Se han completado todas las descargas"
