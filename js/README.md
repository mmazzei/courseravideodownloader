#Javascript section

This folder contains two bookmarklets:

*  `unviewedBookmarklet.js`: extracts the links list for only the unviewed videos.
*  `allBookmarklet.js`: extracts the links list for all the videos.

The scripts `unviewed.js` and `all.js` are the non-minified forms of the bookmarklets. Both compressed with [YUI Compressor](http://developer.yahoo.com/yui/compressor/).
