javascript:(function(){
allTheVideos = $(".course-item-list-section-list li");
downloadLinks = "";
allTheVideos.each(function(index) {
    name = $(".lecture-link", this).text().trim();

    // Escapes the name and append an index to sort easily
    name = ("0" + index).slice(-2) + ' - ' + name.replace(/[^\w\s]+/gi, '');
    videoUrl = $("a[title='Video (MP4)']", this).attr('href');
    subtitlesUrl = $("a[title='Subtitles (srt)']", this).attr('href');
    downloadLinks += videoUrl + ';' + name + '.mp4\n';
    downloadLinks += subtitlesUrl + ';' + name + '.srt\n';
});
alert(downloadLinks);
})();
