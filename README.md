# Coursera video downloader

##Installation

1. Link the script in `sh/` from one of the `PATH` locations:
    sudo ln -s /home/the-user/theFolder/sh/descargame.sh /ush/bin/descargame
1. Create a bookmarklet in your web browser with the scripts `js/unviewedBookmarklet.js` and `js/allBookmarklet.js`.
1. Install the web browser add-on to get the `cookies.txt` ([Chrome](https://chrome.google.com/webstore/detail/cookietxt-export/lopabhfecdfhgogdbojmaicoicjekelh/details) or [Firefox](https://addons.mozilla.org/es/firefox/addon/export-cookies/), supported).

> Note: The cookies.txt file enables wget to download the files from sites that, for example, requires a logged in account.

##Usage

1. Go to the lectures section for the course.
1. Execute the bookmarklet and copy the alert content into a file and named it links.txt (or as you want).
1. Execute the add-on to get the cookies.txt content and copy it into a file named cookies.txt (or as you want).
1. Execute the script:
    descargame -i links.txt -d destinationPath -c cookies.txt

> Note: If the internet connection is interrupted, the download automatically resume from the last point when reconnect. If some of the files already exists, there will not be re-donwloaded.
